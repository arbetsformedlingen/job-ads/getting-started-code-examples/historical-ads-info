## Historical Ads


# 2022-02-03
- Better performance in file handling with explicit garbage collection

# 2022-01-31: First Beta release
- Ads 2016-2021 indexed and used by https://dev-historical-api.jobtechdev.se
- First version of frontend  https://staging-historical-search.jobtechdev.se/
- Files uploaded to https://data.jobtechdev.se/annonser/historiska/berikade/index.html

