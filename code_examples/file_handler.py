import json
import logging
import gc

logging.basicConfig(level="INFO")
log = logging.getLogger(__name__)


def read_jsonlines_file(file_name):
    data = []
    with open(file_name, 'r', encoding='utf-8') as data_file:
        log.info(f"opened file {file_name}")
        while line := data_file.readline():
            data.append(json.loads(line))
    log.info(f"Read {len(data)} ads from '{file_name}'")
    return data


def write_json_lines_file(data, filename):
    log.info(f"Write {len(data)} ads to {filename}")
    with open(filename, "w", encoding='utf-8') as outfile:
        for item in data:
            json.dump(item, outfile, ensure_ascii=False)
            outfile.write('\n')
    log.info(f"Write to {filename} completed")


def write_list_to_file(filename, results):
    """
    Takes a list as param
    writes each item in the list on a separate row
    """
    log.info(f"Writing {len(results)} to {filename}")
    with open(filename, 'w') as f:
        for row in results:
            f.write(f"{row}\n")
    log.info("Write complete")


class FileHandlerWithGC():
    """
    Load file, do garbage collection when leaving scope
    """

    def __init__(self, file_name):
        self.file_name = file_name
        self.ads = read_jsonlines_file(self.file_name)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, exc_traceback):
        del self.ads
        gc.collect()
        log.info(f"Finished with file {self.file_name}")
