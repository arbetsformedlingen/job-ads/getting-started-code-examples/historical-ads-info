import sys
import logging
import file_handler

logging.basicConfig(level="INFO")
log = logging.getLogger(__name__)

timestamps_file = 'timestamps.txt'
output_file = 'ads_per_month.csv'


def get_publication_dates(ads):
    log.info(f"Collecting publication dates from {len(ads)} ads")
    date_stamps = []
    for ad in ads:
        if pub_date := ad.get('publication_date', None):
            date_stamps.append(pub_date[0:10])
    log.info(f"Got {len(date_stamps)} dates with remote_work: True")
    return date_stamps


def does_ad_meet_criteria(ad):
    """
    TODO: This is where you write your code to decide if the ad should be included in the statistics or not
    :param ad:
    :return: True or False
    """
    # example:
    return ad.get('remote_work', False)  # the value can be True or False. If key does not exist, False is returned


def create_statistics_from_timestamps(timestamps_list):
    """
    Builds a dictionary from the items in the timestamps list.
    Timestamp format in file: YYYY-MM-DD
    Only YYYY-MM is used in this example
    """
    log.info(f"Create statistics dictionary from {len(timestamps_list)} entries")
    stats_dict = {}
    for t in timestamps_list:
        year = t[0:4]
        month = t[5:7]
        if y := stats_dict.get(year, None):
            # year exists, check if key for month exists
            if y.get(month, None):
                # key for month exists, increment value
                stats_dict[year][month] += 1
            else:
                # key for month did not exist, add it with starting value of 1
                stats_dict[year][month] = 1
        else:
            # key for year did not exist, hence there can be no sub-key for month.
            # Add year and month with starting value of 1
            stats_dict[year] = {}
            stats_dict[year][month] = 1
    log.info("Created statistics dictionary")
    return stats_dict


def format_data(stats_dict):
    log.info('Formatting data')
    rows_to_write = []
    sorted_years = dict(sorted(stats_dict.items()))
    for year, results in sorted_years.items():
        sorted_months = dict(sorted(results.items()))
        for month, value in sorted_months.items():
            row = f"{year},{month},{value}"
            log.debug(row)
            rows_to_write.append(row)
    log.info(f"Formatted {len(rows_to_write)} rows")
    return rows_to_write


def load_ads_and_get_pub_dates(file_list):
    log.info(f"Files: {file_list}")
    all_publication_dates = []
    for f in file_list:
        with file_handler.FileHandlerWithGC(f) as fh:
            publication_dates = get_publication_dates(fh.ads)
            all_publication_dates.extend(publication_dates)

    file_handler.write_list_to_file(timestamps_file, all_publication_dates)


def get_timestamps():
    with open(timestamps_file) as f:
        timestamps_list = f.readlines()
    log.info(f"Loaded {len(timestamps_list)} from {timestamps_file}")
    return timestamps_list


if __name__ == '__main__':
    """
    Takes a number of file(s) as args 
    For each file:
     Loop through all ads and save publication date if they meet some criteria (you need to write that logic)
    Save publication dates to a file
    
    The point of having a separate file for what has been extracted from the ads is that 
    reading all ads is simple but time-consuming. Whereas doing something with the 
    extracted data is often more complex and requires multiple tries. 
    This will be much easier and quicker with a smaller file. So when you have run it the first time, 
    you can then skip reading of the yearly files and work with extracted data. 
    
    Read publication date file and create a dictionary where keys are created as they are found and 
    values are incremented when needed.
    
    Writes a csv file with 'year, month, number of ads' per row, sorted by year, then by month
    """

    load_ads_and_get_pub_dates(sys.argv[1:])

    timestamps_list = get_timestamps()
    stats = create_statistics_from_timestamps(timestamps_list)
    results = format_data(stats)
    file_handler.write_list_to_file(output_file, results)
