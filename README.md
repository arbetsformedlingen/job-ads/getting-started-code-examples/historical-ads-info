# historical-ads-info

Historical ads from Arbetsförmedlingen 2016-2021.  
The ads are available through an api, in a search page as well as in various file formats.
Ads been converted to current format and [enriched](https://gitlab.com/af-group/jobtech-jobad-enrichments).

## Status

### *First beta version*

## Updates

See `CHANGELOG.md`

## Getting started

### Files or API?

In many cases, the files are a better option than the API.

Do you want a lot of ads? Use the files and filter out what you need from them.

Do you need to find a specific ad, or a small subset of ads? Use the search api.



### Simple search

[Simple search page](https://staging-historical-search.jobtechdev.se/)

### Advanced search (Swagger)

[Swagger: https://dev-historical-api.jobtechdev.se](https://historical.api.jobtechdev.se/)

### API

[API: https://dev-historical-api.jobtechdev.se](https://historical.api.jobtechdev.se/)

### Files

see `Files.md`

## Limitations

- Currently the years 2016-2023 (april 30) are included.
- There are duplicates in the files. Ads have been republished and in those cases the id will be found in multiple ads. The id the ad had when it was published is found in `original_id`, and a new, unique, id is created using a checksum of several fields.
- Response in frontend has a limited number of fields, but has links to more info including the full json response from the api.
- There is a small risk that ads that were published during these years are missing now. It's not likely but we mention it in case you intend to make decisions based on the presence or absence of a specific ad. 
- Missing fields (e.g. no 'description') in some ads from the early years.


## What's next?



### "Deeper" or "broader"? We haven't decided yet. Give us your feedback. What do you need the most?
- Higher data quality in the already released years (2016-2023)?
- Historical ads from earlier years?
- Something else?

## Support, questions, suggestions, planned work
### Let us know what you need
- Use our [forum](https://forum.jobtechdev.se/c/vara-api-er-dataset/historiska-jobb/30)
- Create issues in this GitLab project
- Our [Gitlab issue board](https://gitlab.com/groups/arbetsformedlingen/-/boards?scope=all&label_name[]=Historical%20Ads) has information on planned work.  



## License

- Ads are licensed with CC0. 
- Source code is licensed with Apache 2. 
